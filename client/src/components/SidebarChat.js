import { Avatar } from "@material-ui/core";
import React from "react";
import "../SidebarChat.css";

function SidebarChat() {
  return (
    <div className="sidebar-chat">
      <Avatar />
      <div className="sidebar-chat-info">
        <h3>Channel Name</h3>
        <p>Last message sent</p>
        <small>Timestamp</small>
      </div>
    </div>
  );
}

export default SidebarChat;
