import { IconButton } from "@material-ui/core";
import React, { useState } from "react";
import "../Chat.css";
import MicIcon from "@material-ui/icons/Mic";

function Chat() {
  const [message, setMessage] = useState("");

  const sendMessage = (e) => {
    e.preventDefault();
    // Firebase code goes here.
  };
  return (
    <div className="chat">
      {/* Header */}
      <div className="chat-header">
        <h4>
          To: <span className="chat-name">Channel Name</span>{" "}
        </h4>
        <strong>details</strong>
      </div>
      {/* Body */}
      <div className="chat-body">
        <h2>Messages will go here!</h2>
        {/* <h2>I am a message</h2>
        <h2>I am a message</h2> */}
      </div>
      <div className="chat-input">
        <form>
          <input
            type="text"
            placeholder="Send Message . . ."
            value={message}
            onChange={(e) => setMessage(e.target.value)}
          />
          <button onClick={sendMessage}>Send Message</button>
        </form>
        <IconButton>
          <MicIcon className="chat-mic" />
        </IconButton>
      </div>
    </div>
  );
}

export default Chat;
