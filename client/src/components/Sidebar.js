import React from "react";
import "../Sidebar.css";
import { Avatar, IconButton } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import RateReviewOutlinedIcon from "@material-ui/icons/RateReviewOutlined";
import SidebarChat from "./SidebarChat";
function Sidebar() {
  return (
    <div className="sidebar">
      <div className="sidebar-header">
        <Avatar className="sidebar-avatar" />
        <div className="sidebar-input">
          <SearchIcon />
          <input placeholder="Search" />
        </div>
        <IconButton className="sidebar-input-button" variant="outlined">
          <RateReviewOutlinedIcon />
        </IconButton>
      </div>
      <div className="sidebar-chats">
        <SidebarChat />
        <SidebarChat />
        <SidebarChat />
        <SidebarChat />
        <SidebarChat />
      </div>
    </div>
  );
}

export default Sidebar;
